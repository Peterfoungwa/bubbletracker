import numpy as np
import cv2
from utils.centroidtracker import CentroidTracker
from utils.imageutils import cropImage, getPixelsToMillimeterFactor, \
    getCalibrationConversionFaktorDroplets
from utils.videoutils import getFirstFrame
import time
import pandas as pd
import os

showVideo = True
bg_subtraction = False
limitFrames = True

framelimit = 1000

threshold_maxArea = 1000
threshold_minArea = 1

bottomborder = 0
sideborder = 0

basefolder = "/Users/Sebastian/tubCloud/Uni/Masterarbeit Umweltchemie/Bilder/Tropfen/"
experimentSubfolder = "Zitronensäure/Idex 2um/100FPS/"  # Always with / at
# the end

datafolder = "/Users/Sebastian/tubCloud/Uni/Masterarbeit Umweltchemie/Daten/Tropfen/"
dataExportFolder = datafolder + experimentSubfolder

if os.path.exists(dataExportFolder) is False:
    os.makedirs(dataExportFolder)

folder = basefolder + experimentSubfolder
export = folder + "processed_short/"

if os.path.exists(export) is False:
    os.mkdir(export)

# Read Calibration File
calibrationPath = folder + 'calibration.txt'
conversionFactor, bottomBorder, leftBorder, rightBorder = getCalibrationConversionFaktorDroplets(
    calibrationPath)

for video in os.scandir(folder):
    if video.path.endswith("Kal.MP4"):
        continue
    if (video.path.endswith(".MP4") or video.path.endswith(
            ".mp4")) and video.is_file():
        print('processing file: ' + str(os.path.basename(video)))

        # Mean Otsu Threshold:
        mean_threshold = []
        cap = cv2.VideoCapture(video.path)
        currentFrame = 1
        while currentFrame <= framelimit:
            ret, frame = cap.read()
            if frame is None:
                break
            rotated = cv2.rotate(frame, cv2.ROTATE_90_COUNTERCLOCKWISE)
            height = rotated.shape[0]
            width = rotated.shape[1]

            rotated = rotated[:height - bottomBorder,
                      leftBorder:width - rightBorder]
            original = rotated.copy()

            (H, W) = rotated.shape[:2]

            # Convert to grayscale
            gray = cv2.cvtColor(rotated, cv2.COLOR_BGR2GRAY)

            # Blur Image
            # blur = cv2.bilateralFilter(gray, 5, 21, 21)
            blur = cv2.GaussianBlur(gray, (9, 9), 1)
            ret1, _ = cv2.threshold(blur, 0, 255,
                                    cv2.THRESH_BINARY + cv2.THRESH_OTSU)
            mean_threshold.append(ret1)
            currentFrame += 1
            cap.release()
        mean_threshold = np.mean(mean_threshold)
        print(mean_threshold)

        cap = cv2.VideoCapture(video.path)
        ct = CentroidTracker(maxDisappeared=2, maxJumpDistance=200,
                             trackerType='droplets')
        (H, W) = (None, None)

        fps = int(cap.get(cv2.CAP_PROP_FPS))
        height = int(cap.get(4))
        width = int(cap.get(3))
        if not limitFrames:
            frameLimit = int(cap.get(cv2.CAP_PROP_FRAME_COUNT))
        print("Video resolution is ", width, " by ", height)
        testSize = getFirstFrame(video.path)
        testSize = cv2.rotate(testSize, cv2.ROTATE_90_COUNTERCLOCKWISE)
        height = testSize.shape[0]
        width = testSize.shape[1]
        testSize = testSize[:height - bottomBorder,
                   leftBorder:width - rightBorder]
        y_ZeroOffset = testSize.shape[0]
        print(y_ZeroOffset)
        ct.setCalibrationParameters(conversionFactor, y_ZeroOffset)

        exportFilename = export + str(
            os.path.basename(video).replace(".MP4", ".avi"))
        videoExport = cv2.VideoWriter(exportFilename,
                                      cv2.VideoWriter_fourcc('M', 'J', 'P',
                                                             'G'), fps, (
                                      int(3 * testSize.shape[1]),
                                      int(testSize.shape[0])))

        if bg_subtraction:
            backgroundSubtraction = cv2.createBackgroundSubtractorMOG2()

        framecounter = 1
        # while frame is not None:
        while framecounter <= framelimit:
            ret, frame = cap.read()

            # Check if end of File is reached
            if frame is None:
                break
            # rotate frames

            rotated = cv2.rotate(frame, cv2.ROTATE_90_COUNTERCLOCKWISE)
            height = rotated.shape[0]
            width = rotated.shape[1]

            rotated = rotated[:height - bottomBorder,
                      leftBorder:width - rightBorder]
            original = rotated.copy()

            (H, W) = rotated.shape[:2]

            # Convert to grayscale
            gray = cv2.cvtColor(rotated, cv2.COLOR_BGR2GRAY)

            # Blur Image
            # blur = cv2.bilateralFilter(gray, 5, 21, 21)
            blur = cv2.GaussianBlur(gray, (9, 9), 1)

            ret1, binary = cv2.threshold(blur, 20, 255, cv2.THRESH_TOZERO)
            binary = cv2.adaptiveThreshold(binary, 255,
                                           cv2.ADAPTIVE_THRESH_GAUSSIAN_C, \
                                     cv2.THRESH_BINARY, 21, -15)


            # Apply Background subtraction to frame
            if bg_subtraction:
                foregroundMask = backgroundSubtraction.apply(binary)
            else:
                foregroundMask = binary

            # find Contours
            contours, hierarchy = cv2.findContours(foregroundMask,
                                                   cv2.RETR_LIST,
                                                   cv2.CHAIN_APPROX_SIMPLE)[-2:]
            rectangles = []
            diameters = []
            for cnt in contours:
                if cv2.contourArea(
                        cnt) <= threshold_maxArea and cv2.contourArea(
                        cnt) > threshold_minArea:
                    startx, starty, w, h = cv2.boundingRect(cnt)
                    endx = startx + w
                    endy = starty + h

                    rect = cv2.minAreaRect(cnt)
                    width, height = rect[1]
                    diam = min(width, height)

                    rectangles.append([startx, starty, endx, endy, diam])
                    box = cv2.boxPoints(rect)
                    box = np.int0(box)
                    cv2.drawContours(rotated, [box], 0, (0, 255, 0), 2)

            objects = ct.update(rectangles)
            ct.updateTrajectories()
            ct.updateDiameters()
            ct.estimatePosition()
            ct.storeLastPosition()
            foregroundMask = cv2.cvtColor(foregroundMask, cv2.COLOR_GRAY2BGR)
            for (objectID, centroid) in objects.items():
                # draw both the ID of the object and the centroid of the
                # object on the output frame
                # text = "ID {}".format(objectID)
                # cv2.putText(rotated, text, (centroid[0] - 10, centroid[1] - 10),
                # cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 255, 0), 2)
                cv2.circle(foregroundMask, (centroid[0], centroid[1]), 3,
                           ct.colors[objectID], -1)

            binary = cv2.cvtColor(binary, cv2.COLOR_GRAY2BGR)
            # Create Image array
            numpy_horizontal = np.hstack((original, rotated, foregroundMask))

            if showVideo:
                # Display Frame
                cv2.imshow('frame', numpy_horizontal)

            videoExport.write(numpy_horizontal)

            if ct.analyzedFrames % 50 == 0:
                print("Frames processed: ", str(ct.analyzedFrames))
            if cv2.waitKey(1) & 0xFF == ord('q'):
                break
            framecounter += 1
            # time.sleep(0.1)

        # Release capture when done
        videoExport.release()
        cap.release()
        if bg_subtraction:
            del backgroundSubtraction
        cv2.destroyAllWindows()

        ct.exportTrajectories(str(os.path.basename(video)).replace(".MP4",
                                                                   "") + "_trajectories.csv",
                              dataExportFolder)
        ct.exportDiameters(
            str(os.path.basename(video)).replace(".MP4", "") + "_diameters.csv",
            dataExportFolder, conversionFactor)
