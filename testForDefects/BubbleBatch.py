import cv2
import pandas as pd
from utils.imageutils import *
import os
import math

# threshold_Area 10um
#threshold_Area = 50
#threshold_convexityDefects = 150

# threshold_Area dubiose Fritten = 250
threshold_Area = 50
threshold_convexityDefects = 350
threshold_radii_equality = 20
threshold_circle_proximity = 10

borderwidth = 100
heightCropBorder = 0

maxDiameter = 800

rpmRange = np.arange(90, 91, 10)

basefolder = "/Users/Sebastian/tubCloud/Uni/Masterarbeit Umweltchemie/Bilder/"


experimentType = "Luftblasen/"
experimentName = "Idex 2um/Run3/" # Always with / at the end
picturePath = basefolder + experimentType + experimentName

datafolder = "/Users/Sebastian/tubCloud/Uni/Masterarbeit Umweltchemie/Daten/"
dataExportFolder = datafolder + experimentType + experimentName

if os.path.exists(dataExportFolder) is False:
    os.makedirs(dataExportFolder)

folder = basefolder + experimentName
errors = []
numProcessedFiles = 0

for rpm in rpmRange:
    print("Processing ", str(rpm))
    listOfDiameters = pd.DataFrame()
    path = picturePath + str(rpm)
    export = path + "/processed"

    if os.path.exists(export) is False:
        os.mkdir(export)

    # Create empty list to hold picture names to reconstruct column names after concatenation (column names are lost there)
    pictureNames = []

    for picture in os.scandir(path):
        if (picture.path.endswith(".JPG") or picture.path.endswith(".jpg")) and picture.is_file():
            numProcessedFiles += 1
            print("Processing File:", str(os.path.basename(picture)))
            image = cv2.imread(picture.path)
            cropped = cropRotate(image)

            height = cropped.shape[0]
            width = cropped.shape[1]

            mmPerPixel = 80 / height # Conversion from width in pixel to mm. Screen is 500 mm wide

            minusBorder = cropped[borderwidth + heightCropBorder:height - borderwidth - heightCropBorder, borderwidth:width - borderwidth]

            binary = Convert2Binary(minusBorder)

            if np.mean(binary) == 255:
                print("detection incorrect")
                errors.append(str(picture.path))

            else:
                diameters = []
                contours, hierarchy = cv2.findContours(binary, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)[-2:]
                # print(contours)
                overlaps, singlebubbles = getOverlappingBubbleContours(contours, threshold_Area,
                                                                       threshold_convexityDefects, image = minusBorder)
                for i in singlebubbles:
                    color = (0, 255, 0)
                    diameters.append(mmPerPixel * findAndDrawEllipse(i, minusBorder, color))

                bubbles, circles, recombinated = getoverlappingBubblesSegmentation(overlaps, minusBorder,
                                                                                   threshold_convexityDefects,
                                                                                   threshold_radii_equality,
                                                                                   threshold_circle_proximity)
                for bubble in bubbles:
                    color = (255, 0, 0)
                    diameters.append(mmPerPixel * findAndDrawEllipse(bubble, minusBorder, color))

                for rec in recombinated:
                    MatchType = rec[2]
                    print('MatchType', MatchType)
                    rec = rec[0]
                    val, (p, k), rad = getLeastSquareCircle(rec)
                    if not val:
                        print("least squares circle not successful")
                    center = (int(p), int(k))
                    rad = int(rad)
                    reconstructedCircleArclenght = 2 * math.pi * rad

                    color = (255, 0, 255)
                    if MatchType in ["Ellipse", "None"]:
                        diameters.append(mmPerPixel * findAndDrawEllipse(rec, minusBorder, color))
                    else:
                        diameters.append(mmPerPixel * findAndDrawBubble(rec, minusBorder, color))
                        #diameter = rad * 2 * mmPerPixel
                        #diameters.append(diameter)
                        #cv2.circle(minusBorder, center, rad, color, 3)

                cv2.imwrite(export + '/' + os.path.basename(picture).replace(".JPG", ".JPG"), minusBorder)
                cv2.imwrite(export + '/' + (os.path.basename(picture)).replace(".JPG", "_bin.JPG"), binary)
                diameters = pd.DataFrame({os.path.basename(picture): diameters})
                pictureNames.append(os.path.basename(picture))
                listOfDiameters = pd.concat([listOfDiameters, diameters], ignore_index=True, axis=1)

    print(listOfDiameters)
    csvpath = dataExportFolder + str(rpm) + "rpm.csv"

    # Restore picture names in column headers
    listOfDiameters.columns = pictureNames
    listOfDiameters.to_csv(csvpath, index=False)

print("Detection of " + str(len(errors)) + " in " + str(numProcessedFiles) + " files faulty. Those files were excluded in final results.")
print("List of unprocessed files can be found in:")
print(picturePath + "errors.txt")
with open(picturePath + "errors.txt", "w") as outfile:
    outfile.write("\n".join(errors))