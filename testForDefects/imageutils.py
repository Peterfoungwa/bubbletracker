import cv2
from scipy import signal, optimize
from scipy.special import ellipe
import numpy as np
import math
import functools

def Convert2Binary(image):
    # Convert the image into grayscale image.
    if (len(image.shape) < 3):
        gray = image.copy()
    else:
        gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)

    # Blur Image to remove noise
    blur = cv2.bilateralFilter(gray, 9, 101, 101)
    #blur = cv2.GaussianBlur(gray, (9, 9), 1)

    # Adaptive Thresholding to create binary image
    binary = cv2.adaptiveThreshold(blur, 255, cv2.ADAPTIVE_THRESH_GAUSSIAN_C, cv2.THRESH_BINARY_INV, blockSize=401, C=6)

    # Previous: uncomment dilate operation
    #kernel = cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (7, 7))
    #morph = cv2.dilate(morph, kernel, iterations = 1)
    #morph = cv2.erode(morph, kernel, iterations = 1)
    # Copy the threshold image for floodfilling
    im_floodfill = binary.copy()

    h, w = binary.shape[:2]
    mask = np.zeros((h + 2, w + 2), np.uint8)

    # Floodfill from (0, 0)
    cv2.floodFill(im_floodfill, mask, (0, 0), 255)

    # Invert floofilled image
    im_floodfill_inv = cv2.bitwise_not(im_floodfill)

    newBinary = (binary | im_floodfill_inv)

    # Performing morphological operation to remove noise, using kernel with disk shape as bubbles are elliptical to round
    kernel = cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (7, 7))

    # Previous working:
    # kernel = np.ones((3, 3), np.uint8)
    return cv2.morphologyEx(newBinary, cv2.MORPH_OPEN, kernel)

def getEllipsePerimeter(a,b):
    e_sq = 1.0 - b ** 2 / a ** 2
    return 4 * a * ellipe(e_sq)

def getEquivalentDiameter(longside, shortside):
    # Takes longside and shortside of ellipsoid, calculates its volume and returns the diameter of a sphere with equivalent volume, note that longside and shortside are measured from center to outer shell
    volume = (4/3) * math.pi * pow(longside, 2) * shortside
    radius = pow((3/4 * volume / math.pi), pow(3, -1))
    diameter = radius*2
    return round(diameter, 2)

def findAndDrawBubble(contour, image, col = (255, 255, 0)):
    (x,y), radius = cv2.minEnclosingCircle(contour)
    center = (int(x), int(y))
    radius = int(radius)
    cv2.circle(image, center, radius, col, 4)
    return getEquivalentDiameter(radius, radius)

def findAndDrawEllipse(contour, image, color = (255, 0, 255)): # fitEllipse
    sizeThreshold = 200
    hull = cv2.convexHull(contour)
    # Finding rotated Ellipse needs at least 5 Point
    if hull is not None and len(hull) >= 5:
        ellipse = cv2.fitEllipse(hull)
        origin, dimensions, angle = ellipse

        if dimensions[0] >= dimensions[1]:
            longside = dimensions[0] / 2
            shortside = dimensions[1] / 2
        else:
            shortside = dimensions[0] / 2
            longside = dimensions[1] / 2

        if longside < sizeThreshold and shortside < sizeThreshold:
            try:
                cv2.ellipse(image, ellipse, color, 2)
            except:
                print("Ellipse could not be drawn")
            return getEquivalentDiameter(longside, shortside)

    else:
        x, y, w, h = cv2.boundingRect(hull)

        if w > h:
            longside = w / 2
            shortside = h / 2

        else:
            longside = h / 2
            shortside = w / 2

        center = (int(x + longside), int(y + shortside))
        ellipse = []
        if longside < sizeThreshold and shortside < sizeThreshold:
            cv2.ellipse(image, center, (int(longside), int(shortside)), 0, 0, 360, color, 2)
            return getEquivalentDiameter(longside, shortside)



    print("Contour too big")
    cv2.drawContours(image, contour, -1, (0, 0, 255), 4)
    return math.nan

def cropRotate(image):
    # Convert Image to grayscale
    if (len(image.shape) < 3):
        gray = image.copy()
    else:
        gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)

    blur = cv2.bilateralFilter(gray, 9, 75, 75)
    ret, binary = cv2.threshold(blur, 50, 255, cv2.THRESH_BINARY)
    kernel = np.ones((30, 30), np.uint8)
    binary = cv2.erode(binary, kernel, iterations=1)
    contours, hierarchy = cv2.findContours(binary, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_NONE)[-2:]
    img_box = image.copy()

    if len(contours) != 0:
        cnt = max(contours, key=cv2.contourArea)
        rect = cv2.minAreaRect(cnt)
        box = cv2.boxPoints(rect)
        box = np.int0(box)

        W = rect[1][0]
        H = rect[1][1]

        Xs = [i[0] for i in box]
        Ys = [i[1] for i in box]
        x1 = min(Xs)
        x2 = max(Xs)
        y1 = min(Ys)
        y2 = max(Ys)

        rotated = False
        angle = rect[2]

        if angle < -45:
            angle += 90
            rotated = True

        center = (int((x1 + x2) / 2), int((y1 + y2) / 2))

        mult = 1  # I wanted to show an area slightly larger than my min rectangle set this to one if you don't
        size = (int(mult * (x2 - x1)), int(mult * (y2 - y1)))

        M = cv2.getRotationMatrix2D((size[0] / 2, size[1] / 2), angle, 1.0)

        cropped = cv2.getRectSubPix(img_box, size, center)
        cropped = cv2.warpAffine(cropped, M, size)

        croppedW = W if not rotated else H
        croppedH = H if not rotated else W

        croppedRotated = cv2.getRectSubPix(cropped, (int(croppedW * mult), int(croppedH * mult)),
                                           (size[0] / 2, size[1] / 2))
    return croppedRotated

def getPixelsToMillimeterFactor(RefPixels, RefMillimeter):
    return RefMillimeter / RefPixels

def cropImage(image, borderwidthX, borderwidthY):
    height = image.shape[0]
    width = image.shape[1]
    return image[borderwidthY:height - borderwidthY, borderwidthX:width - borderwidthX]

def getOverlappingBubbleContours(contours, threshold_Area, threshold_convexityDefects, image = None):
    overlappingContours = []
    singleBubbleContours = []
    overlappingDefects = []
    for cnt in contours:
        if len(cnt) < 4:
            continue
        area = cv2.contourArea(cnt)

        # Filter out unwanted artefacts (dirt, etc.)
        if area > threshold_Area:
            #try:
            defectsList, _ = getDefectsBettaieb(cnt)
            for defct in defectsList:
                defct = defct.flatten()
                cv2.circle(image, (defct[0], defct[1]), 2, (0,0,255), 2)
                #defectsList, _ = getLocalMaxDefects(cnt, threshold_convexityDefects)
            #except:
            #    print("Error in handling convexityDefects, contour is not monotonic")
             #   defectsList = None

            # Check for defects
            if defectsList is not None and len(cnt) > 5:
                # Combined Circles contours have at least 2 defects
                if len(defectsList) >= 2:
                    overlappingContours.append(cnt)
                    overlappingDefects.append(defectsList)

                # Only one defect indicates one bubble
                else:
                    singleBubbleContours.append(cnt)
            else:
                singleBubbleContours.append(cnt)
    return overlappingContours, singleBubbleContours

def getLocalMaxDefects(cnt, threshold_convexityDefects):
    distances = []
    hull = cv2.convexHull(cnt, returnPoints=False)
    defects = cv2.convexityDefects(cnt, hull)

    distances = [defect[0, 3] for defect in defects]
    distances = np.array(distances)
    distances = (distances > threshold_convexityDefects) * distances

    localMaxDefectsIndex = signal.argrelextrema(distances, np.greater, order=7)[0]
    localMaxDefects = [defects[i] for i in localMaxDefectsIndex]

    return localMaxDefects, localMaxDefectsIndex

def getoverlappingBubblesSegmentation(overlaps, image, threshold_convexityDefects, threshold_radii_equality, threshold_circle_proximity):
    combinedCircles = []
    recombinedCircles = []
    reconstructedCircles = []
    splitGroupID = 1
    for cnt in overlaps:
        localMaxDefects, localMaxDefectsIndex = getDefectsBettaieb(cnt)
        #localMaxDefects, localMaxDefectsIndex = getLocalMaxDefects(cnt, threshold_convexityDefects)
        # only split contours if more than 1 defect present
        splitContours = []
        if len(localMaxDefectsIndex) > 1:
            splitContours = getSplitContours(cnt, localMaxDefects, localMaxDefectsIndex, image)
        else:
            combinedCircles.append(cnt)
            continue

        centers = []
        radii = []
        validContours = splitContours

        for arc in validContours:
            val, (x, y), radius = getLeastSquareCircle(arc)
            if val:
                center = (int(x), int(y))
                radius = int(radius)
            else:
                center = (0, 0)
                radius = 0

            centers.append(center)
            radii.append(radius)

        # Combine Circles that correspond to matching criteria
        recombined = combineCircles(centers, radii, threshold_circle_proximity, threshold_radii_equality, splitContours)

        for seg in recombined:
            segment, MatchType = seg
            arcLenght = cv2.arcLength(segment, closed=False)
            if MatchType in ['Circle', 'None']:
                val, (p, k), rad = getLeastSquareCircle(segment)
                center = (int(p), int(k))
                rad = int(rad)
                circle = (center, rad)
                reconstructedCircleArclenght = 2 * math.pi * rad
                diffCircle = reconstructedCircleArclenght - arcLenght

                try:
                    origin, dimensions, angle = cv2.fitEllipse(segment)
                    origin, dimensions, angle = cv2.fitEllipse(segment)
                    reconstructedEllpiseArclenght = getEllipsePerimeter(max(dimensions), min(dimensions))
                    diffEllipse = reconstructedEllpiseArclenght - arcLenght

                    if diffCircle < diffEllipse:
                        MatchType = "Circle"
                    else:
                        MatchType = "Ellipse"

                except cv2.error:
                    print('Segment too small to reconstruct Ellipse')
                    pass


                if arcLenght  >= 0.5 * reconstructedCircleArclenght:
                    print("Found valid circle")
                    recombinedCircles.append([segment, splitGroupID, MatchType])

            elif MatchType == 'Ellipse':
                try:
                    origin, dimensions, angle = cv2.fitEllipse(segment)
                except cv2.error:
                    print('Segment too small to reconstruct Ellipse')
                    continue

                reconstructedEllpiseArclenght = getEllipsePerimeter(max(dimensions), min(dimensions))
                if cv2.arcLength(segment, closed=False) >= 0.5 * reconstructedEllpiseArclenght:
                    print("Found valid circle")
                    recombinedCircles.append([segment, splitGroupID, MatchType])

        splitGroupID+=1
    return combinedCircles, reconstructedCircles, recombinedCircles

def getSplitContours(cnt, localMaxDefects, localMaxDefectsIndex, image):
    splitContours = []
    for index in range(len(localMaxDefectsIndex)):
        defectcontourindex = localMaxDefectsIndex[index]
        if not splitContours:
            segment = np.append(cnt[localMaxDefectsIndex[-1]:], cnt[:defectcontourindex], axis=0)
            splitContours.append(segment)
        else:
            splitContours.append(cnt[lastdefectcontourindex:defectcontourindex].copy())
        lastdefectcontourindex = defectcontourindex
    return splitContours

def combineCircles(centers, radii, threshold_circle_proximity, threshold_radii_equality, splitContours):
    checked = []
    mergedContours = []
    MatchType = 'None'
    if len(centers) >= 2 and len(centers) <= 3:
        for c in splitContours:
            mergedContours.append([c, MatchType])
        return mergedContours

    for j in range(len(radii)):
        if j in checked or radii[j] == 0:
            #tempContour = []
            continue
        MatchType = 'None'
        tempContour = splitContours[j]
        for k in range(j + 2, len(radii)):
            if k in checked:
                continue
            diff = abs(radii[k] - radii[j])

            if diff < 0.25 * min(radii[j], radii[k]) and MatchType != 'Ellipse':
                try:
                    distance = abs(np.sqrt((centers[k][0] - centers[j][0])**2 + (centers[k][1] - centers[j][1])**2))
                    if distance < 0.25 * min(radii[j], radii[k]):
                        print("Found Match Circle")
                        MatchType = 'Circle'
                        checked.append(k)
                        tempContour = np.append(tempContour, splitContours[k], axis = 0)
                except TypeError:
                    print('TypeError Exception Raised')

            elif MatchType != 'Circle':
                try:
                    ellipseRef = cv2.fitEllipse(tempContour)
                    ellipseComp = cv2.fitEllipse(splitContours[k])

                except cv2.error:
                    continue
                origin_ref, dimensions_ref, angle_ref = ellipseRef
                origin_comp, dimensions_comp, angle_comp = ellipseComp

                diff = abs(max(dimensions_ref) - max(dimensions_comp))
                if diff < 0.5 * min(max(dimensions_ref), max(dimensions_comp)):
                    try:
                        distance = abs(np.sqrt(
                            (origin_comp[0] - origin_ref[0]) ** 2 + (origin_comp[1] - origin_ref[1]) ** 2))
                        if distance < 0.5 * min(max(dimensions_ref), max(dimensions_comp)):
                            print("Found Match Ellipse")
                            MatchType = 'Ellipse'
                            checked.append(k)
                            tempContour = np.append(tempContour, splitContours[k], axis=0)
                    except TypeError:
                        print('TypeError Exception Raised')
        checked.append(j)
        mergedContours.append([tempContour, MatchType])
    return mergedContours

def getCalibrationConversionFaktor(pathToFile):
    with open(pathToFile, 'r') as f:
        calibrationParameters = f.readlines()

    calibrationWidth_Pixel = int(calibrationParameters[1].split(':')[1].strip())
    calibrationWidth_mm = int(calibrationParameters[2].split(':')[1].strip())
    return getPixelsToMillimeterFactor(calibrationWidth_Pixel, calibrationWidth_mm)


# Decorator to count functions calls
def countcalls(fn):
    "decorator function count function calls "

    @functools.wraps(fn)
    def wrapped(*args):
        wrapped.ncalls += 1
        return fn(*args)

    wrapped.ncalls = 0
    return wrapped

def getLeastSquareCircle(contour):
    x = [i[0, 0] for i in contour]
    y = [i[0, 1] for i in contour]

    if len(x) < 2:
        print('Not enough points in contour')
        val = False
        center = (0, 0)
        radius = 0
        return val, center, radius
    # coordinates of the barycenter
    x_m = np.mean(x)
    y_m = np.mean(y)

    # calculation of the reduced coordinates
    u = x - x_m
    v = y - y_m

    # == METHOD 2b ==
    # Advanced usage, with jacobian
    method_2b = "leastsq with jacobian"

    def calc_R(xc, yc):
        """ calculate the distance of each 2D points from the center c=(xc, yc) """
        return np.sqrt((x - xc) ** 2 + (y - yc) ** 2)

    @countcalls
    def f_2b(c):
        """ calculate the algebraic distance between the 2D points and the mean circle centered at c=(xc, yc) """
        Ri = calc_R(*c)
        return Ri - Ri.mean()

    @countcalls
    def Df_2b(c):
        """ Jacobian of f_2b
        The axis corresponding to derivatives must be coherent with the col_deriv option of leastsq"""
        xc, yc = c
        df2b_dc = np.empty((len(c), len(x)))

        Ri = calc_R(xc, yc)
        df2b_dc[0] = (xc - x) / Ri  # dR/dxc
        df2b_dc[1] = (yc - y) / Ri  # dR/dyc
        df2b_dc = df2b_dc - df2b_dc.mean(axis=1)[:, np.newaxis]

        return df2b_dc

    center_estimate = x_m, y_m
    center_2b, ier = optimize.leastsq(f_2b, center_estimate, Dfun=Df_2b, col_deriv=True)

    xc_2b, yc_2b = center_2b
    Ri_2b = calc_R(xc_2b, yc_2b)
    R_2b = Ri_2b.mean()
    residu_2b = sum((Ri_2b - R_2b) ** 2)
    residu2_2b = sum((Ri_2b ** 2 - R_2b ** 2) ** 2)
    ncalls_2b = f_2b.ncalls

    # Summary
    # print (method_2b, xc_2b, yc_2b, R_2b, ncalls_2b, Ri_2b.std(), residu_2b, residu2_2b)
    center = xc_2b, yc_2b
    val = True
    return val, center, R_2b

def getDefectsBettaieb(contour):
    # Needs CHAIN_APPROX_SIMPLE
    defects = []
    defectsIndex = []
    for i in range(-2, len(contour)-2):
        x_A = contour[i].flatten()[0]
        y_A = contour[i].flatten()[1]

        x_B = contour[i+1].flatten()[0]
        y_B = contour[i+1].flatten()[1]

        x_C = contour[i+2].flatten()[0]
        y_C = contour[i + 2].flatten()[1]

        lineA = ((x_A, y_A), (x_B, y_B))
        lineB = ((x_B, y_B), (x_C, y_C))

        angle1 = ang(lineA[0], lineA[1])
        angle2 = ang(lineB[0], lineB[1])
        x_testPoint = (x_A + x_C) / 2
        y_testPoint = (y_A + y_C) / 2

        angle = abs(angle1 - angle2)
        result = cv2.pointPolygonTest(contour, (x_testPoint, y_testPoint), False)
        print('Angle ges: ', angle, 'Angle1: ', angle1, 'Angle2: ', angle2)
        if angle < 130 and result == -1:
            x_mean = np.mean([x_A, x_B, x_C])
            y_mean = np.mean([y_A, y_B, y_C])

            corr = (x_A - x_mean + x_B - x_mean + x_C - x_mean) * (y_A - y_mean + y_B - y_mean + y_C - y_mean) / (math.sqrt(pow(x_A - x_mean + x_B - x_mean + x_C - x_mean, 2)) * math.sqrt(pow(y_A - y_mean + y_B - y_mean + y_C - y_mean, 2)))
            distance = distanceTwoPoints(contour[i+1].flatten(), contour[i+2].flatten())
            if corr > 0.9 and distance >= 1:
                defects.append(contour[i+1])
                defectsIndex.append(i+1)
    return defects, defectsIndex

def distanceTwoPoints(a, b):
    return math.sqrt((b[0] - a[0])**2 + (b[1] - a[1])**2)

def ang(point1, point2):
    x1, y1 = point1
    x2, y2 = point2
    dx = x2 - x1
    dy = y2 - y1
    rads = math.atan2(-dy, dx)
    return math.degrees(rads)

def full_from_sparse(contour):
    horizontal = np.array([1, 0], 'int')
    vertical = np.array([0, 1], 'int')
    diagonal = np.array([1, 1], 'int')
    def _get_points(p0, p1):
        # find all points on line connecting p0 and p1,
        # including p0, excluding p1
        # line must be horizontal, vertical or diagonal
        diff = p1-p0
        if np.max(np.abs(diff)) <= 1:
            # p0 and p1 are neighbor points
            # or duplicate points, i.e.g no in-between points
            return [p0]
        if diff[0] == 0:
            # vertical
            fac = diff[1]
            inc = vertical
        elif diff[1] == 0:
            # horizontal
            fac = diff[0]
            inc = horizontal
        elif diff[0] == diff[1]:
            # diagonal
            fac = diff[0]
            inc = diagonal
        else:
            raise Exception("points not connected", p0, p1)
        return [p0 + _fac*inc for _fac in range(0, fac, np.sign(fac))]

    full = []
    points = contour[:, 0, :]
    for i in range(len(points)-1):
        _points = _get_points(points[i], points[i+1])
        full.extend(_points)

    # add points from last segment, endpoint to startpoint
    _points = _get_points(points[-1], points[0])
    full.extend(_points)

    # reshape as contour
    full = np.array(full, dtype='int')
    rows, cols = full.shape
    return full.reshape(rows, 1, cols)