import cv2
import pandas as pd
from utils.imageutils import *
#from utils.imutils_working_presentation import *
import os
import numpy as np
import cv2
from utils.centroidtracker import CentroidTracker
from utils.videoutils import getThresholdParameters
import pandas as pd
from random import randrange
#test
showVideo = True
bg_subtraction = False
limitFrames = True

threshold_convexityDefects = 350
threshold_radii_equality = 3
threshold_circle_proximity = 3
threshold_Area = 10
threshold_minArea = 4
threshold_maxArea = 200

frameLimit = 500

rpmRange = np.arange(10, 91, 10)

basefolder = "/Users/Sebastian/tubCloud/Uni/Masterarbeit Umweltchemie/Bilder/Luftblasen/"
experimentSubfolder = "Neu/Idex 20um/100FPS/" # Always with / at the end

datafolder = "/Users/Sebastian/tubCloud/Uni/Masterarbeit Umweltchemie/Daten/Luftblasen/"
dataExportFolder = datafolder + experimentSubfolder

if os.path.exists(dataExportFolder) is False:
    os.makedirs(dataExportFolder)

folder = basefolder + experimentSubfolder

export = folder + "/processed_short/"

if os.path.exists(export) is False:
    os.mkdir(export)

# Read Calibration File
calibrationPath = folder + 'calibration.txt'
conversionFactor = getCalibrationConversionFaktor(calibrationPath)

for video in os.scandir(folder):
    if video.path.endswith("Kal.MP4"):
        continue
    if video.path.endswith(".MP4") or video.path.endswith(".mp4") and video.is_file():
        print('processing file: ' + str(os.path.basename(video)))
        cap = cv2.VideoCapture(video.path)
        fps = int(cap.get(cv2.CAP_PROP_FPS))
        #height = int(cap.get(4))
        height = 710-10
        widthMax = 900
        widthMin = 300
        #width = int(cap.get(3)) \
        width = widthMax - widthMin
        if not limitFrames:
            frameLimit = int(cap.get(cv2.CAP_PROP_FRAME_COUNT))
        print("Video resolution is ", width, " by ", height)
        threshold_maxArea, threshold_minArea, threshold_convexityDefects = getThresholdParameters(height)
        exportFilename = export + str(os.path.basename(video).replace(".MP4", ".avi"))
        videoExport = cv2.VideoWriter(exportFilename, cv2.VideoWriter_fourcc('M','J','P','G'), fps, (3*height, width))

        if bg_subtraction:
            backgroundSubtraction = cv2.createBackgroundSubtractorMOG2()

        ct = CentroidTracker(maxDisappeared=4, maxJumpDistance=300, trackerType='droplets')
        ct.setCalibrationParameters(conversionFactor)
        (H, W) = (None, None)
        bursts = []
        currentFrame = 1
        #while frame is not None:
        while currentFrame <= frameLimit:
            # Capture frame by frame
            ret, frame = cap.read()

            # Check if end of File is reached
            if frame is None:
                break

            # rotate frames
            rotated = cv2.rotate(frame, cv2.ROTATE_90_COUNTERCLOCKWISE)[widthMin:widthMax,10:710]
            (H, W) = rotated.shape[:2]

            # Convert to grayscale
            gray = cv2.cvtColor(rotated, cv2.COLOR_BGR2GRAY)

            # Blur Image
            #blur = cv2.GaussianBlur(gray, (9, 9), 1)
            # binary = cv2.adaptiveThreshold(upright, 255, cv2.ADAPTIVE_THRESH_GAUSSIAN_C, cv2.THRESH_BINARY, 101, 2)
            #ret1, binary1 = cv2.threshold(blur, 90, 255, cv2.THRESH_BINARY)
            binary = Convert2Binary(gray)
            #et1, binary1 = cv2.threshold(blur,0,255,cv2.THRESH_BINARY+cv2.THRESH_OTSU)

            tried = []
            count = 0
            escape = False
            while np.mean(binary) >= 245:
                if count > 100:
                    escape = True
                    break
                newWidthCoordinate = randrange(width)
                if newWidthCoordinate in tried:
                    continue
                newOrigin = (0, newWidthCoordinate)
                binary = Convert2Binary(gray, newOrigin)
                tried.append(newWidthCoordinate)
                count += 1
            if escape:
                continue

            # Morphological Operations
            # morph = cv2.morphologyEx(binary, cv2.MORPH_OPEN, kernel=cv2.getStructuringElement(cv2.MORPH_ELLIPSE,(3,3)))

            # Apply Background subtraction to frame
            if bg_subtraction:
                foregroundMask = backgroundSubtraction.apply(binary)
            else:
                foregroundMask = binary

            # find Contours
            contours, hierarchy = cv2.findContours(foregroundMask, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_NONE)[-2:]

            # Convert binary to RGB for drawing colors
            binary = cv2.cvtColor(binary, cv2.COLOR_GRAY2BGR)
            foregroundMask = cv2.cvtColor(foregroundMask, cv2.COLOR_GRAY2BGR)

            #split, mergeCandidates = splitOverlaps(contours, binary, threshold_convexityDefects, threshold_minArea, 4)
            overlaps, singlebubbles = getOverlappingBubbleContours(contours, threshold_Area, threshold_convexityDefects)

            # imgKeyPoints = cv2.drawKeypoints(upright,
            # keypoints, np.array([]), (0, 0, 255),
            # cv2.DrawMatchesFlags_DRAW_RICH_KEYPOINTS)
            rectangles = []

            for cnt in singlebubbles:
                area = cv2.contourArea(cnt)
                if threshold_maxArea >= area > threshold_minArea:
                    startX, startY, w, h = cv2.boundingRect(cnt)
                    endX = startX + w
                    endY = startY + h
                    rectangles.append([startX, startY, endX, endY, 0, area])
                    cv2.rectangle(foregroundMask, (startX, startY), (startX + w, startY + h), (0, 255, 0), 2)
                    # findAndDrawBubble(cnt, binary)

            overlappingCircles, _, reconstructedCircles = getoverlappingBubblesSegmentation(overlaps, foregroundMask, threshold_convexityDefects, threshold_radii_equality, threshold_circle_proximity, type='Surface')
            for cnt in overlappingCircles:
                area = cv2.contourArea(cnt)
                if threshold_maxArea >= area > threshold_minArea:
                    startX, startY, w, h = cv2.boundingRect(cnt)
                    endX = startX + w
                    endY = startY + h
                    rectangles.append([startX, startY, endX, endY, 0, area])
                    cv2.rectangle(foregroundMask, (startX, startY), (startX + w, startY + h), (0, 255, 0), 2)

            for rec in reconstructedCircles:
                cnt, id, _ = rec
                area = cv2.contourArea(cnt)
                if threshold_maxArea >= area > threshold_minArea:
                    startX, startY, w, h = cv2.boundingRect(cnt)
                    endX = startX + w
                    endY = startY + h
                    rectangles.append([startX, startY, endX, endY, id, area])
                    cv2.rectangle(foregroundMask, (startX, startY), (startX + w, startY + h), (0, 255, 0), 2)
                    # findAndDrawBubble(cnt, binary)


            objects = ct.update(rectangles)
            bursts.append(ct.burstperFrame)
            ct.updateTrajectories()
            ct.updateDiameters()
            ct.estimatePosition()
            ct.storeLastPosition()

            if objects is not None:
                for (objectID, centroid) in objects.items():
                    # draw both the ID of the object and the centroid of the
                    # object on the output frame
                    text = "ID {}".format(objectID)
                    # cv2.putText(rotated, text, (centroid[0] - 10, centroid[1] - 10),
                    #            cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 255, 0), 2)
                    # cv2.circle(rotated, (centroid[0], centroid[1]), 4, (0, 0, 255), -1)
                    cv2.circle(foregroundMask, (centroid[0], centroid[1]), 3, ct.colors[objectID], -1)

            if len(binary.shape) < 3:
                binary = cv2.cvtColor(binary, cv2.COLOR_GRAY2BGR)

            if len(foregroundMask.shape) < 3:
                foregroundMask = cv2.cvtColor(foregroundMask, cv2.COLOR_GRAY2BGR)

            # Create Image array
            numpy_horizontal = np.hstack((rotated, binary, foregroundMask))

            if showVideo:
                # Display Frame
                cv2.imshow('frame', numpy_horizontal)

            videoExport.write(numpy_horizontal)
            #  if slowDownBy:
            #     time.sleep(slowDownBy)

            if ct.analyzedFrames % 50 == 0:
                print("Frames processed: ", str(ct.analyzedFrames))
            if cv2.waitKey(1) & 0xFF == ord('q'):
                break
            # time.sleep(0.1)
            currentFrame+=1

        # Release capture when done
        videoExport.release()
        cap.release()
        if bg_subtraction:
            del backgroundSubtraction
        cv2.destroyAllWindows()

        dataframe = pd.DataFrame(bursts)
        basefilename = str(os.path.basename(video)).replace(".MP4", "")
        dataframe.to_csv(dataExportFolder + basefilename + "_geplatzeperSekunde.csv")

        ct.exportBurstDiameters(basefilename + "_burstDiameters.csv", dataExportFolder, conversionFactor)
        ct.exportTrajectories(basefilename + "_trajectories.csv", dataExportFolder)
        ct.exportDiameters(basefilename + "_diameters.csv", dataExportFolder, conversionFactor)
