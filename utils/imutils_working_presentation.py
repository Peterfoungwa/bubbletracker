from scipy.spatial import distance as dist
from collections import OrderedDict
import cv2
import numpy as np
import math
from scipy import signal
import random
from skimage.feature import peak_local_max

def Convert2Gray(image):
    if (len(image.shape) < 3):
        gray = image.copy()
    else:
        gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    return gray

def Convert2BinaryParams(image, blocksize, C):

    # Adaptive Thresholding to create binary image
    binary = cv2.adaptiveThreshold(image, 255, cv2.ADAPTIVE_THRESH_GAUSSIAN_C, cv2.THRESH_BINARY_INV, blocksize, C)

    morph = binary.copy()

    # Performing morphological operation to remove noise, using kernel with disk shape as bubbles are elliptical to round
    kernel = cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (11,11))
    morph = cv2.morphologyEx(morph, cv2.MORPH_OPEN, kernel)

    # Previous: uncomment dilate operation
    kernel = cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (11,11))
    morph = cv2.morphologyEx(morph, cv2.MORPH_CLOSE, kernel)

    # Copy the threshold image for floodfilling
    im_floodfill = morph.copy()

    h, w = morph.shape[:2]
    mask = np.zeros((h + 2, w + 2), np.uint8)

    # Floodfill from (0, 0)
    cv2.floodFill(im_floodfill, mask, (0, 0), 255)

    # Invert floofilled image
    im_floodfill_inv = cv2.bitwise_not(im_floodfill)

    return (morph | im_floodfill_inv)

def Convert2Binary(image):
    # Convert the image into grayscale image.
    if (len(image.shape) < 3):
        gray = image.copy()
    else:
        gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)

    # Blur Image to remove noise
    blur = cv2.bilateralFilter(gray, 9, 101, 101)
    #blur = cv2.GaussianBlur(gray, (9, 9), 0)

    # Adaptive Thresholding to create binary image
    binary = cv2.adaptiveThreshold(blur, 255, cv2.ADAPTIVE_THRESH_GAUSSIAN_C, cv2.THRESH_BINARY_INV, blockSize=71, C=6)

    # Performing morphological operation to remove noise, using kernel with disk shape as bubbles are elliptical to round
    kernel = cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (5,5))

    # Previous working:
    # kernel = np.ones((3, 3), np.uint8)
    morph = cv2.morphologyEx(binary, cv2.MORPH_OPEN, kernel)

    # Previous: uncomment dilate operation
    kernel = cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (7, 7))
    morph = cv2.dilate(morph, kernel, iterations = 1)
    morph = cv2.erode(morph, kernel, iterations = 1)

    # Copy the threshold image for floodfilling
    im_floodfill = morph.copy()

    h, w = morph.shape[:2]
    mask = np.zeros((h + 2, w + 2), np.uint8)

    # Floodfill from (0, 0)
    cv2.floodFill(im_floodfill, mask, (0, 0), 255)

    # Invert floofilled image
    im_floodfill_inv = cv2.bitwise_not(im_floodfill)

    return (morph | im_floodfill_inv)

def getRelevantDefects(rawDefects, threshold_convexityDefects):
    defectslist = []
    if rawDefects is not None:
        for i in range(rawDefects.shape[0]):
         # Get start, end, far, distance for each defect, only distance and far points are used
            s, e, f, d = rawDefects[i, 0]

            # Only major defects ( >= convexityDefectsThreshold) are significant for combined bubbles
            if d >= threshold_convexityDefects:
                defectslist.append(f)
    return defectslist

def FindContours(binary):
    contours, hierarchy = cv2.findContours(binary, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
    return contours, hierarchy

def splitContourIterative(cnt, defectslist, threshold_convexityDefects, threshold_Area, image, maxIterations):

    # This function loops over the defects list and extracts one bubble for each iteration, until there is only one left
    cntRemainder = cnt
    bubblediameters = []
    iteration = 0
    drawlastcontour = True

    while len(defectslist) > 1 and cv2.contourArea(cntRemainder) > threshold_Area:
        # Split first bubble from contour
        splitBubbleCnt = np.append(cntRemainder[:defectslist[0]], cntRemainder[defectslist[-1]:], axis=0)

        # New Contour without first bubble
        cntRemainder = cntRemainder[defectslist[0]:defectslist[-1]]

        # Draw bubble and find diameter
        bubblediameters.append(findAndDrawEllipse(splitBubbleCnt, image))

        # Find new hull and new convexity Defects
        try:
            hull = cv2.convexHull(cntRemainder, returnPoints=False)
            defects = cv2.convexityDefects(cntRemainder, hull)
            defectslist = getRelevantDefects(defects, threshold_convexityDefects)
        except:
            print("Error in handling convexityDefects, contour is not monotonic")
            cv2.drawContours(image, cntRemainder, -1, (0, 0, 255), 5)
            drawlastcontour = False
            break

        # Emergency Stop
        iteration += 1
        if iteration > maxIterations:
            print("Iterations in contour splitting exceeded maximum value of " + str(maxIterations))
            break

    # All Intersections are resolved, now its time to draw the last bubble
    if drawlastcontour:
        bubblediameters.append(findAndDrawEllipse(cntRemainder, image))

    return bubblediameters

def splitOverlaps(contours, image, threshold_convexityDefects, threshold_Area, maxIterations):
    splitContours = []
    mergeCandidates = []

    for cnt in contours:
        area = cv2.contourArea(cnt)

        # Filter out unwanted artefacts (dirt, etc.)
        if area > threshold_Area:
            try:
                hull = cv2.convexHull(cnt, returnPoints=False)
                defects = cv2.convexityDefects(cnt, hull)
            except:
                print("Error in handling convexityDefects, contour is not monotonic")
                defects = None

            # Check for defects
            if defects is not None and len(cnt) > 5:
                defectsList = getRelevantDefects(defects, threshold_convexityDefects)

                # Combined Circles contours have at least 2 defects
                if len(defectsList) >= 2:

                    # Mark combined Circles in Image
                    x, y, w, h = cv2.boundingRect(cnt)
                    cv2.rectangle(image, (x, y), (x + w, y + h), (0, 0, 255), 2)

                    # Split Contour on Convexity Defects, Defects are considered to either of the touching contour
                    #overlappingContours = splitContourAtDefects(cnt, defectsList, threshold_convexityDefects, threshold_Area, image, 4)

                    overlappingContours = getSplitPoints(cnt, defectsList, threshold_convexityDefects, threshold_Area, image)

                    for subcnt in overlappingContours:
                        splitContours.append(subcnt)
                        mergeCandidates.append(subcnt)

                # Only one defect indicates one bubble
                else:
                    splitContours.append(cnt)
            else:
                splitContours.append(cnt)
    return splitContours, mergeCandidates

def splitContourAtDefects(cnt, defectslist, threshold_convexityDefects, threshold_Area, image, maxIterations):

    # This function loops over the defects list and extracts one bubble for each iteration, until there is only one left
    cntRemainder = cnt
    iteration = 0
    drawlastcontour = True

    # Initialize list to store individual contours after splitting
    splitContours = []

    while len(defectslist) > 1 and cv2.contourArea(cntRemainder) > threshold_Area:

        # Split first bubble from contour
        splitBubbleCnt = np.append(cntRemainder[:defectslist[0]], cntRemainder[defectslist[-1]:], axis=0)

        # New Contour without first bubble
        cntRemainder = cntRemainder[defectslist[0]:defectslist[-1]]

        # Repair both contours
        splitBubbleCnt = cv2.approxPolyDP(splitBubbleCnt, 0.1, False)
        cntRemainder = cv2.approxPolyDP(cntRemainder, 2, False)

        # Append to contour list for export
        splitContours.append(splitBubbleCnt)

        # Find new hull and new convexity Defects
        try:
            hull = cv2.convexHull(cntRemainder, returnPoints=False)
            defects = cv2.convexityDefects(cntRemainder, hull)
            defectslist = getRelevantDefects(defects, threshold_convexityDefects)

        except:
            print("Error in handling convexityDefects, contour is not monotonic")
            cv2.drawContours(image, cntRemainder, -1, (0, 0, 255), 5)
            drawlastcontour = False
            break

        # Emergency Stop
        iteration += 1
        if iteration > maxIterations:
            print("Iterations in contour splitting exceeded maximum value of " + str(maxIterations))
            break

    # All Intersections are resolved, now its time to draw the last bubble
    if drawlastcontour:
        splitContours.append(cntRemainder)
    return splitContours

def getEquivalentDiameter(longside, shortside):
    # Takes longside and shortside of ellipsoid, calculates its volume and returns the diameter of a sphere with equivalent volume, note that longside and shortside are measured from center to outer shell
    volume = (4/3) * math.pi * pow(longside, 2) * shortside
    radius = pow((3/4 * volume / math.pi), pow(3, -1))
    diameter = radius*2
    return round(diameter, 2)

def getSplitPoints(contour, defects, threshold_convexityDefects, threshold_Area, image):
    BubbbleContours = []

    remainingContour = contour
    remainingContourDefects = defects
    count_iterations = 0

    # as long as there are defects in the remaining Contour
    while len(remainingContourDefects) > 1 and cv2.contourArea(remainingContour) > threshold_Area:

        # Split contour at global maximal defects for each hull line and extract first segment
        segment = np.append(remainingContour[remainingContourDefects[-1]:], remainingContour[:remainingContourDefects[0]], axis=0)
        remainingContour = remainingContour[remainingContourDefects[0]:remainingContourDefects[-1]]

        # Check if first segment and remaininContour contain local maxima defects, which were overridden by global defect
        try:
            segmentHull = cv2.convexHull(segment, returnPoints=False)
            segmentDefects = getRelevantDefects(cv2.convexityDefects(segment, segmentHull), threshold_convexityDefects)
        except:
            print("Could not get hull for segment")
            print(segment)
            break
        # If there are more defects in segment, do further splitting
        while len(segmentDefects) > 2 and cv2.contourArea(segment) > threshold_Area and len(segment) > 5:

            # segment = np.append(segment[:segmentDefects[0]], segment[segmentDefects[-1]:], axis=0)
            segment = segment[:segmentDefects[0]]

            remainingContour = np.insert(remainingContour, 0, segment[segmentDefects[0]:], axis=0)

            # Check if segment contains local maxima defects, which were overridden by previous global defect
            try:
                segmentHull = cv2.convexHull(segment, returnPoints=False)
                segmentDefects = getRelevantDefects(cv2.convexityDefects(segment, segmentHull), threshold_convexityDefects)
            except:
                segmentDefects = []
                print("Could not get hull for segment")
                cv2.drawContours(image, [segment], 0, (0, 255, 0), 3)
                print(segment)

            count_iterations += 1

            if count_iterations > 15:
                print("Too many iterations in segment splitting")
                try:
                    cv2.drawContours(image, [remainingContour], 0, (0, 255, 0), 3)
                except:
                    pass
                break

        # Escape Loop when there is no more defects
        BubbbleContours.append(segment)

        # Calculate new defects
        if len(remainingContour) > 5:
            try:
                remainingContourHull = cv2.convexHull(remainingContour, returnPoints=False)
                remainingContourDefects = getRelevantDefects(cv2.convexityDefects(remainingContour, remainingContourHull),
                                                        threshold_convexityDefects)
            except:
                print("Could not get hull for remaining contour")
                print(remainingContour)
                cv2.drawContours(image, [remainingContour], 0, (0, 255, 0), 3)
                remainingContourDefects = []
                break
        else:
            remainingContourDefects = []

    BubbbleContours.append(remainingContour)
    return BubbbleContours

def findAndDrawBubble(contour, image):
    (x,y), radius = cv2.minEnclosingCircle(contour)
    center = (int(x), int(y))
    radius = int(radius)
    cv2.circle(image, center, radius, (0, 255, 0), 4)
    return getEquivalentDiameter(radius, radius)

def findAndDrawEllipse(contour, image):
    sizeThreshold = 350
    hull = cv2.convexHull(contour)
    # Finding rotated Ellipse needs at least 5 Point
    if hull is not None and len(hull) >= 5:
        ellipse = cv2.fitEllipse(hull)
        try:
            cv2.ellipse(image, ellipse, (255, 0, 255), 2)
        except:
            print("Ellipse could not be drawn")

        origin, dimensions, angle = ellipse

        if dimensions[0] >= dimensions[1]:
            longside = dimensions[0] / 2
            shortside = dimensions[1] / 2
        else:
            shortside = dimensions[0] / 2
            longside = dimensions[1] / 2

    else:
        x, y, w, h = cv2.boundingRect(hull)

        if w > h:
            longside = w / 2
            shortside = h / 2

        else:
            longside = h / 2
            shortside = w / 2

        center = (int(x + longside), int(y + shortside))
        ellipse = []
        cv2.ellipse(image, center, (int(longside), int(shortside)), 0, 0, 360, (255, 0, 255), 2)

    if longside < sizeThreshold and shortside < sizeThreshold:
        return getEquivalentDiameter(longside, shortside)

    print("Contour too big")
    cv2.drawContours(image, contour, -1, (0, 0, 255), 4)
    return math.nan

def cropRotate(image):
    # Convert Image to grayscale
    if (len(image.shape) < 3):
        gray = image.copy()
    else:
        gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)

    blur = cv2.bilateralFilter(gray, 9, 75, 75)
    ret, binary = cv2.threshold(blur, 50, 255, cv2.THRESH_BINARY)
    kernel = np.ones((30, 30), np.uint8)
    binary = cv2.erode(binary, kernel, iterations=1)
    contours, hierarchy = cv2.findContours(binary, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_NONE)[-2:]
    img_box = image.copy()

    if len(contours) != 0:
        cnt = max(contours, key=cv2.contourArea)
        rect = cv2.minAreaRect(cnt)
        box = cv2.boxPoints(rect)
        box = np.int0(box)

        W = rect[1][0]
        H = rect[1][1]

        Xs = [i[0] for i in box]
        Ys = [i[1] for i in box]
        x1 = min(Xs)
        x2 = max(Xs)
        y1 = min(Ys)
        y2 = max(Ys)

        rotated = False
        angle = rect[2]

        if angle < -45:
            angle += 90
            rotated = True

        center = (int((x1 + x2) / 2), int((y1 + y2) / 2))

        mult = 1  # I wanted to show an area slightly larger than my min rectangle set this to one if you don't
        size = (int(mult * (x2 - x1)), int(mult * (y2 - y1)))

        M = cv2.getRotationMatrix2D((size[0] / 2, size[1] / 2), angle, 1.0)

        cropped = cv2.getRectSubPix(img_box, size, center)
        cropped = cv2.warpAffine(cropped, M, size)

        croppedW = W if not rotated else H
        croppedH = H if not rotated else W

        croppedRotated = cv2.getRectSubPix(cropped, (int(croppedW * mult), int(croppedH * mult)),
                                           (size[0] / 2, size[1] / 2))
    return croppedRotated

def pixelsToMillimeter(pixels, RefPixels, RefMillimeter):
    factor = RefMillimeter / RefPixels
    return (pixels * factor)

def getPixelsToMillimeterFactor(RefPixels, RefMillimeter):
    factor = RefMillimeter / RefPixels
    return factor

def analyzeContours(contours, image, threshold_Area, threshold_convexityDefects):
    diameters = []
    for cnt in contours:
        area = cv2.contourArea(cnt)

        # Filter out unwanted artefacts (dirt, etc.)
        if area > threshold_Area:
            hull = cv2.convexHull(cnt, returnPoints=False)
            defects = cv2.convexityDefects(cnt, hull)

            # Check for defects
            if defects is not None:
                defectsList = getRelevantDefects(defects, threshold_convexityDefects)

                # Combined Circles contours have at least 2 defects
                if len(defectsList) >= 2:

                    # Mark combined Circles in Image
                    x, y, w, h = cv2.boundingRect(cnt)
                    cv2.rectangle(image, (x, y), (x + w, y + h), (0, 0, 255), 2)

                    # Split Contour on Convexity Defects, Defects are considered to either of the touching contour
                    splitBubbleDiameters = splitContourIterative(cnt, defectsList, threshold_convexityDefects, threshold_Area, image, 4)
                    diameters.extend(splitBubbleDiameters)

                # Only one bubble
                else:
                    diameter = findAndDrawEllipse(cnt, image)
                    diameters.append(diameter)

    return diameters

def analyzeContours2(contours, image, threshold_Area, maxDiameter):
    diameters = []
    for cnt in contours:
        area = cv2.contourArea(cnt)
        # Filter out unwanted artefacts (dirt, etc.)
        if area > threshold_Area:
            diameter = findAndDrawEllipse(cnt, image)
            if diameter < maxDiameter:
                diameters.append(diameter)
    return diameters

def cropImage(image, borderwidthX, borderwidthY):
    height = image.shape[0]
    width = image.shape[1]
    return image[borderwidthY:height - borderwidthY, borderwidthX:width - borderwidthX]

def stackImages(scale,imgArray):
    rows = len(imgArray)
    cols = len(imgArray[0])
    rowsAvailable = isinstance(imgArray[0], list)
    width = imgArray[0][0].shape[1]
    height = imgArray[0][0].shape[0]
    if rowsAvailable:
        for x in range ( 0, rows):
            for y in range(0, cols):
                if imgArray[x][y].shape[:2] == imgArray[0][0].shape [:2]:
                    imgArray[x][y] = cv2.resize(imgArray[x][y], (0, 0), None, scale, scale)
                else:
                    imgArray[x][y] = cv2.resize(imgArray[x][y], (imgArray[0][0].shape[1], imgArray[0][0].shape[0]), None, scale, scale)
                if len(imgArray[x][y].shape) == 2: imgArray[x][y]= cv2.cvtColor( imgArray[x][y], cv2.COLOR_GRAY2BGR)
        imageBlank = np.zeros((height, width, 3), np.uint8)
        hor = [imageBlank]*rows
        hor_con = [imageBlank]*rows
        for x in range(0, rows):
            hor[x] = np.hstack(imgArray[x])
        ver = np.vstack(hor)
    else:
        for x in range(0, rows):
            if imgArray[x].shape[:2] == imgArray[0].shape[:2]:
                imgArray[x] = cv2.resize(imgArray[x], (0, 0), None, scale, scale)
            else:
                imgArray[x] = cv2.resize(imgArray[x], (imgArray[0].shape[1], imgArray[0].shape[0]), None,scale, scale)
            if len(imgArray[x].shape) == 2: imgArray[x] = cv2.cvtColor(imgArray[x], cv2.COLOR_GRAY2BGR)
        hor= np.hstack(imgArray)
        ver = hor
    return ver

def getRelevantDefectsNeu(rawDefects, threshold_convexityDefects):
    defectslist = []
    if rawDefects is not None:
        for i in range(rawDefects.shape[0]):
         # Get start, end, far, distance for each defect, only distance and far points are used
            s, e, f, d = rawDefects[i, 0]

            # Only major defects ( >= convexityDefectsThreshold) are significant for combined bubbles
            if d >= threshold_convexityDefects:
                defectslist.append(rawDefects[i, 0])
    return defectslist

def getOverlappingBubbleContours(contours, threshold_Area, threshold_convexityDefects):
    overlappingContours = []
    singleBubbleContours = []
    overlappingDefects = []
    for cnt in contours:
        area = cv2.contourArea(cnt)

        # Filter out unwanted artefacts (dirt, etc.)
        if area > threshold_Area:
            try:
                hull = cv2.convexHull(cnt, returnPoints=False)
                defects = cv2.convexityDefects(cnt, hull)
            except:
                print("Error in handling convexityDefects, contour is not monotonic")
                defects = None

            # Check for defects
            if defects is not None and len(cnt) > 5:
                defectsList = getRelevantDefects(defects, threshold_convexityDefects)

                # Combined Circles contours have at least 2 defects
                if len(defectsList) >= 2:
                    overlappingContours.append(cnt)
                    overlappingDefects.append(defectsList)

                # Only one defect indicates one bubble
                else:
                    singleBubbleContours.append(cnt)
            else:
                singleBubbleContours.append(cnt)
    return overlappingContours, singleBubbleContours

def overlappingBubblesSegmentation(overlaps, image, threshold_convexityDefects, threshold_radii_equality, threshold_circle_proximity):
    diameters = []
    for cnt in overlaps:
        distances = []
        hull = cv2.convexHull(cnt, returnPoints=False)
        defects = cv2.convexityDefects(cnt, hull)

        for defect in defects:
            distances.append(defect[0, 3])

        distances = np.array(distances)
        distances = (distances > threshold_convexityDefects) * distances
        localMaxDefectsIndex = signal.argrelextrema(distances, np.greater, order= 7)[0]

        localMaxDefects = []

        for i in localMaxDefectsIndex:
            localMaxDefects.append(defects[i])
            #point = tuple(cnt[defects[i, 0][2]][0])
            #cv2.circle(morph, point, 6, (255, 0, 0),3)

        # only split contours if more than 1 defect present
        splitContours = []
        if len(localMaxDefectsIndex) > 1:
            for index in range(len(localMaxDefectsIndex)):
                defectcontourindex = localMaxDefects[index][0, 2]
                if not splitContours:
                    splitContours.append(np.append(cnt[localMaxDefects[-1][0, 2]:], cnt[:defectcontourindex], axis = 0))
                else:
                    splitContours.append(cnt[lastdefectcontourindex:defectcontourindex])
                lastdefectcontourindex = defectcontourindex
        else:
            diameters.append(findAndDrawBubble(cnt, image))

        centers = []
        radii = []

        for arc in range(len(splitContours)):
            #color = (np.random.randint(255), np.random.randint(255), np.random.randint(255))
            #cv2.drawContours(morph, splitContours[arc], -1, color, 4)

            (x, y), radius = cv2.minEnclosingCircle(splitContours[arc])
            center = (int(x), int(y))
            centers.append(center)

            radius = int(radius)
            radii.append(radius)

        checked = []
        segmentJoinIndices = np.array([0 for x in splitContours])

        for j in range(len(radii)):
            if j not in checked and j < len(radii):
                for k in range(j+1, len(radii)):
                    dist = abs(radii[k] - radii[j])
                    if dist < threshold_radii_equality:
                        segmentJoinIndices[k] = j
                        checked.append(k)
                    else:
                        segmentJoinIndices[k] = k
                checked.append(j)
                segmentJoinIndices[j] = j

        checkedCenters = []
        combinedCircles = []
        for l in range(len(centers)):
            candidatesIndex = np.where(segmentJoinIndices == l)[0]
            if candidatesIndex.any() > 0:
                for candidate in range(len(candidatesIndex)):
                    matchFound = False
                    if candidatesIndex[candidate] not in checkedCenters and candidate < len(candidatesIndex):
                        for nextCandidate in range(candidate + 1, len(candidatesIndex)):
                            distance = math.sqrt(((centers[candidatesIndex[candidate]][0] - centers[candidatesIndex[nextCandidate]][0]) ** 2) + ((centers[candidate][1] - centers[nextCandidate][1]) ** 2))
                            if distance < threshold_circle_proximity:
                                matchFound = True
                                combinedCircles.append(np.append(splitContours[candidatesIndex[candidate]], splitContours[candidatesIndex[nextCandidate]], axis=0))
                                checkedCenters.append(candidatesIndex[nextCandidate])
                                print("Found Match")
                        checkedCenters.append(candidatesIndex[candidate])
                        if not matchFound:
                            combinedCircles.append(splitContours[candidatesIndex[candidate]])

        for bubble in combinedCircles:
            diameters.append(findAndDrawBubble(bubble, image))
    return diameters


def getoverlappingBubblesSegmentation(overlaps, image, threshold_convexityDefects, threshold_radii_equality, threshold_circle_proximity):
    diameters = []
    combinedCircles = []
    exportCombined = []
    for cnt in overlaps:
        distances = []
        hull = cv2.convexHull(cnt, returnPoints=False)
        defects = cv2.convexityDefects(cnt, hull)

        for defect in defects:
            distances.append(defect[0, 3])

        distances = np.array(distances)
        distances = (distances > threshold_convexityDefects) * distances
        localMaxDefectsIndex = signal.argrelextrema(distances, np.greater, order= 7)[0]

        localMaxDefects = []

        for i in localMaxDefectsIndex:
            localMaxDefects.append(defects[i])
            #point = tuple(cnt[defects[i, 0][2]][0])
            #cv2.circle(morph, point, 6, (255, 0, 0),3)

        # only split contours if more than 1 defect present
        splitContours = []
        if len(localMaxDefectsIndex) > 1:
            for index in range(len(localMaxDefectsIndex)):
                defectcontourindex = localMaxDefects[index][0, 2]
                if not splitContours:
                    splitContours.append(np.append(cnt[localMaxDefects[-1][0, 2]:], cnt[:defectcontourindex], axis = 0))
                else:
                    splitContours.append(cnt[lastdefectcontourindex:defectcontourindex])
                lastdefectcontourindex = defectcontourindex
        #else:
            #diameters.append(findAndDrawBubble(cnt, image))

        centers = []
        radii = []

        for arc in range(len(splitContours)):
            #color = (np.random.randint(255), np.random.randint(255), np.random.randint(255))
            #cv2.drawContours(morph, splitContours[arc], -1, color, 4)

            (x, y), radius = cv2.minEnclosingCircle(splitContours[arc])
            center = (int(x), int(y))
            centers.append(center)

            radius = int(radius)
            radii.append(radius)

        checked = []
        segmentJoinIndices = np.array([0 for x in splitContours])

        for j in range(len(radii)):
            if j not in checked and j < len(radii):
                for k in range(j+1, len(radii)):
                    dist = abs(radii[k] - radii[j])
                    if dist < threshold_radii_equality:
                        segmentJoinIndices[k] = j
                        checked.append(k)
                    else:
                        segmentJoinIndices[k] = k
                checked.append(j)
                segmentJoinIndices[j] = j

        checkedCenters = []
        combinedCircles = []
        for l in range(len(centers)):
            candidatesIndex = np.where(segmentJoinIndices == l)[0]
            if candidatesIndex.any() > 0:
                for candidate in range(len(candidatesIndex)):
                    matchFound = False
                    if candidatesIndex[candidate] not in checkedCenters and candidate < len(candidatesIndex):
                        for nextCandidate in range(candidate + 1, len(candidatesIndex)):
                            distance = math.sqrt(((centers[candidatesIndex[candidate]][0] - centers[candidatesIndex[nextCandidate]][0]) ** 2) + ((centers[candidate][1] - centers[nextCandidate][1]) ** 2))
                            if distance < threshold_circle_proximity:
                                matchFound = True
                                combinedCircles.append(np.append(splitContours[candidatesIndex[candidate]], splitContours[candidatesIndex[nextCandidate]], axis=0))
                                checkedCenters.append(candidatesIndex[nextCandidate])
                                print("Found Match")
                        checkedCenters.append(candidatesIndex[candidate])
                        if not matchFound:
                            combinedCircles.append(splitContours[candidatesIndex[candidate]])

        #for bubble in combinedCircles:
            #diameters.append(findAndDrawBubble(bubble, image))

        exportCombined.append(combinedCircles)
    return combinedCircles
