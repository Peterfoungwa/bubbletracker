from math import pi, sqrt

def getHalfSphereVolume(radius):
    return 2/3 * radius**3 * pi

def getDiameterofHalfSphere(volume):
    radius = pow((3/2 * volume / pi), pow(3, -1))
    diameter = radius*2
    return round(diameter, 2)

def equivalentDiameterArea(area):
    return sqrt(area / pi) * 2