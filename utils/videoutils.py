import cv2
import numpy as np
def getThresholdParameters(frameHeight):
    if frameHeight == 1080:
        threshold_maxArea = 500
        threshold_minArea = 20
        threshold_convexityDefects = 400

    if frameHeight == 2160:
        threshold_maxArea = 500
        threshold_minArea = 20
        threshold_convexityDefects = 400

    else:
        threshold_maxArea = 300
        threshold_minArea = 3
        threshold_convexityDefects = 350
    return (threshold_maxArea, threshold_minArea, threshold_convexityDefects)

def getFirstFrame(videofile):
    vidcap = cv2.VideoCapture(videofile)
    success, image = vidcap.read()
    if success:
        return image
    else:
        print('empty file')


def getMeanThreshold(videoPath, fameLimit):
    # Mean Otsu Threshold:
    mean_threshold = []
    cap = cv2.VideoCapture(videoPath)
    currentFrame = 1
    while currentFrame <= fameLimit:
        ret, frame = cap.read()
        if frame is None:
            break
        gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
        # Blur Image
        blur = cv2.GaussianBlur(gray, (9, 9), 1)
        ret1, _ = cv2.threshold(blur, 0, 255,
                                cv2.THRESH_BINARY + cv2.THRESH_OTSU)
        mean_threshold.append(ret1)
        currentFrame += 1
        cap.release()
    return  np.mean(mean_threshold)